﻿using System;

using Foundation;
using UIKit;
using CoreGraphics;
namespace Alex.iOS
{
	public partial class BiometricCell : UITableViewCell
	{
		

		public static readonly NSString Key = new NSString("BiometricCell");
		public static readonly UINib Nib;
		UILabel lblCurrentValue;
		PickerType selectedPickerType;
		static BiometricCell()
		{
			Nib = UINib.FromName("BiometricCell", NSBundle.MainBundle);
		}

		protected BiometricCell(IntPtr handle) : base(handle)
		{
			
			// Note: this .ctor should not contain any initialization logic.
		}


		//partial void adjustValueForSlider(UISlider sender)
		//{
			
		//	CGRect trackRect = slider.TrackRectForBounds(slider.Bounds);
		//	CGRect thumbRect = slider.ThumbRectForBounds(slider.Bounds,trackRect,slider.Value);

		//	lblCurrentValue.Center = new CGPoint(thumbRect.Width / 2, lblCurrentValue.Center.Y);
		//	//CGRect trackRect = [self.slider trackRectForBounds: self.slider.bounds];
		//	//CGRect thumbRect = [self.slider thumbRectForBounds: self.slider.bounds

		//	//			   trackRect: trackRect

		//	//				   value: self.slider.value];
		//	//throw new NotImplementedException();
		//	//    yourLabel.center = CGPointMake(thumbRect.origin.x + self.slider.frame.origin.x,  self.slider.frame.origin.y - 20);

		//}

		public override void AwakeFromNib()
		{
			base.AwakeFromNib();
			nfloat kFONT_WIDTH_FACTOR = UIScreen.MainScreen.Bounds.Width / 414;
			slider.SetThumbImage(UIImage.FromBundle("circle"),UIControlState.Normal);

			UIView heightPaddingView = new UIView();
			heightPaddingView.Frame = new CoreGraphics.CGRect(0, 0, 10, txtWeight.Frame.Size.Height);
			txtHeight.LeftView = heightPaddingView;
			txtHeight.LeftViewMode = UITextFieldViewMode.Always;

			UIView weightPaddingView = new UIView();
			weightPaddingView.Frame = new CoreGraphics.CGRect(0, 0, 10, txtWeight.Frame.Size.Height);
			txtWeight.LeftView = weightPaddingView;
			txtWeight.LeftViewMode = UITextFieldViewMode.Always;

			UIView agePaddingView = new UIView();
			agePaddingView.Frame = new CoreGraphics.CGRect(0, 0, 10, txtWeight.Frame.Size.Height);
			txtAge.LeftView = agePaddingView;
			txtAge.LeftViewMode = UITextFieldViewMode.Always;


			UIView leanPaddingView = new UIView();
			leanPaddingView.Frame = new CoreGraphics.CGRect(0, 0, 10, txtWeight.Frame.Size.Height);
			txtLeanMuscle.LeftView = leanPaddingView;
			txtLeanMuscle.LeftViewMode = UITextFieldViewMode.Always;


			NSNotificationCenter.DefaultCenter.RemoveObserver(this);
			NSNotificationCenter.DefaultCenter.AddObserver ((NSString)"SetBiometricPickerData", SetPickerData);
//NSNotificationCenter.DefaultCenter.PostNotificationName((NSString)"showPickerView", null);




			lblCurrentValue = new UILabel();
			lblCurrentValue.Frame = new CoreGraphics.CGRect(0, slider.Frame.Bottom, 20, 20);
			lblCurrentValue.BackgroundColor = UIColor.Red;
			//slider.Superview.AddSubview(lblCurrentValue);


			slider.MinValue = 0;
			slider.MaxValue = 10;
			slider.Value = 5;
			lblSliderValue.Text = "5";
		CGRect trackRect = slider.TrackRectForBounds(slider.Bounds);
		CGRect thumbRect = slider.ThumbRectForBounds(slider.Bounds, trackRect, slider.Value);

			//lblCurrentValue.Center = new CGPoint(thumbRect.Width / 2, lblCurrentValue.Center.Y);


			slider.ValueChanged += HandleValueChanged;

			txtHeight.Font = UIFont.FromName("Arial",kFONT_WIDTH_FACTOR * 12f);
			txtWeight.Font = UIFont.FromName("Arial",kFONT_WIDTH_FACTOR * 12f);
			txtAge.Font = UIFont.FromName("Arial",kFONT_WIDTH_FACTOR * 12f);
			txtLeanMuscle.Font = UIFont.FromName("Arial",kFONT_WIDTH_FACTOR * 12f);
			btnLowerBody.TitleLabel.Font = UIFont.FromName("Arial",kFONT_WIDTH_FACTOR * 12f);
			btnUpperBody.TitleLabel.Font = UIFont.FromName("Arial",kFONT_WIDTH_FACTOR * 12f);

			Console.WriteLine("");

			btnLowerBody.SetNeedsLayout();
			btnUpperBody.SetNeedsLayout();
			btnLowerBody.SetNeedsLayout();
			btnUpperBody.SetNeedsLayout();

			btnLowerBody.ImageEdgeInsets = new UIEdgeInsets(0, btnLowerBody.Frame.Size.Width - 40, 0,0);
			btnUpperBody.ImageEdgeInsets = new UIEdgeInsets(0, btnUpperBody.Frame.Size.Width - 40, 0,0);
		}



		void HandleValueChanged(object sender, EventArgs e)
		{   // display the value in a label
			//label.Text = slider.Value.ToString();

			CGRect trackRect = slider.TrackRectForBounds(slider.Bounds);
			CGRect thumbRect = slider.ThumbRectForBounds(slider.Bounds, trackRect, slider.Value);

			lblCurrentValue.Center = new CGPoint(slider.Value * slider.Bounds.Size.Width, 40);

			lblSliderValue.Text = Convert.ToString((int)slider.Value);

			//lblCurrentValue.Center = new CGPoint(thumbRect.Width / 2, lblCurrentValue.Center.Y);
			//CGRect trackRect = [self.slider trackRectForBounds: self.slider.bounds];
			//CGRect thumbRect = [self.slider thumbRectForBounds: self.slider.bounds

			//			   trackRect: trackRect

			//				   value: self.slider.value];
			//throw new NotImplementedException();
			//    yourLabel.center = CGPointMake(thumbRect.origin.x + self.slider.frame.origin.x,  self.slider.frame.origin.y - 20);

		}

		public void setInitialData()
		{

				
		}

		public void SetPickerData(NSNotification notification)
		{
			NSNotificationCenter.DefaultCenter.RemoveObserver(this);
			if (notification.Object == null)
			{
				return;
			}
			Console.Write("PICKER DATA");
			NSString title = (NSString)notification.Object;
			if (selectedPickerType == PickerType.LowerBodyStrength)
			{
				btnLowerBody.SetTitle(title, UIControlState.Normal);
			}
			else if (selectedPickerType == PickerType.UpperBodyStrength)
			{
				btnUpperBody.SetTitle(title, UIControlState.Normal);
			}
		}
		partial void BiometricPickerView(UIButton sender)
		{
			int tag = (int)sender.Tag;
			selectedPickerType = (PickerType)tag;
			NSNotificationCenter.DefaultCenter.PostNotificationName((NSString)"showPickerView", NSNumber.FromNInt(sender.Tag));

		}
	}
}
