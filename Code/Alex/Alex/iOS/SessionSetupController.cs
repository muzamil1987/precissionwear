﻿using System;

using UIKit;
using Foundation;
namespace Alex.iOS
{
	public partial class SessionSetupController : UIViewController
	{
		partial void BtnBack_Activated(UIBarButtonItem sender)
		{
			this.NavigationController.PopViewController(true);
			//throw new NotImplementedException();
		}

		


		public SessionSetupController(IntPtr handle) : base(handle)
		{
		}
		public SessionSetupController() : base("SessionSetupController", null)
		{
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			string[] tableItems = new string[] { "Vegetables" };
			tblListing.Source = new TableSourceSession(tableItems);

			// Perform any additional setup after loading the view, typically from a nib.
		}

		partial void AddSessionPressed(UIButton sender)
		{
			UIStoryboard addNewGeneralSb = UIStoryboard.FromName("Main", null);
			SessionDetailController addNewGeneralSbVC = (SessionDetailController)addNewGeneralSb.InstantiateViewController("SessionDetailController");
			NavigationController.PushViewController(addNewGeneralSbVC, true);
		}

		public override void DidReceiveMemoryWarning()
		{
			base.DidReceiveMemoryWarning();
			// Release any cached data, images, etc that aren't in use.
		}
	}

public class TableSourceSession : UITableViewSource
{
	string[] TableItems;
	static NSString MyCellId = new NSString("sessionCell");

	public TableSourceSession(string[] items)
	{

		TableItems = items;
	}

	public override nint RowsInSection(UITableView tableview, nint section)
	{
		return TableItems.Length;
	}

	public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
	{


		var cell = (UITableViewCell)tableView.DequeueReusableCell(MyCellId, indexPath);
		string item = TableItems[indexPath.Row];
		//cell.TextLabel.Text = item;
		cell.SelectionStyle = UITableViewCellSelectionStyle.None;
		//cell.setInitialData();
		//cell.SetThumbImage(UIImage.FromFile("29_icon.png"), UIControlState.Normal);
		return cell;
	}
}
}

