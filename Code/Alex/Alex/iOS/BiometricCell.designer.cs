// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace Alex.iOS
{
	[Register ("BiometricCell")]
	partial class BiometricCell
	{
		[Outlet]
		UIKit.UIButton btnLowerBody { get; set; }

		[Outlet]
		UIKit.UIButton btnUpperBody { get; set; }

		[Outlet]
		UIKit.UIImageView imgProfile { get; set; }

		[Outlet]
		UIKit.UILabel lblCity { get; set; }

		[Outlet]
		UIKit.UILabel lblName { get; set; }

		[Outlet]
		UIKit.UILabel lblSliderValue { get; set; }

		[Outlet]
		UIKit.UISlider slider { get; set; }

		[Outlet]
		UIKit.UITextField txtAge { get; set; }

		[Outlet]
		UIKit.UITextField txtHeight { get; set; }

		[Outlet]
		UIKit.UITextField txtLeanMuscle { get; set; }

		[Outlet]
		UIKit.UITextField txtWeight { get; set; }

		[Action ("BiometricPickerView:")]
		partial void BiometricPickerView (UIKit.UIButton sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (lblSliderValue != null) {
				lblSliderValue.Dispose ();
				lblSliderValue = null;
			}

			if (btnLowerBody != null) {
				btnLowerBody.Dispose ();
				btnLowerBody = null;
			}

			if (btnUpperBody != null) {
				btnUpperBody.Dispose ();
				btnUpperBody = null;
			}

			if (imgProfile != null) {
				imgProfile.Dispose ();
				imgProfile = null;
			}

			if (lblCity != null) {
				lblCity.Dispose ();
				lblCity = null;
			}

			if (lblName != null) {
				lblName.Dispose ();
				lblName = null;
			}

			if (slider != null) {
				slider.Dispose ();
				slider = null;
			}

			if (txtAge != null) {
				txtAge.Dispose ();
				txtAge = null;
			}

			if (txtHeight != null) {
				txtHeight.Dispose ();
				txtHeight = null;
			}

			if (txtLeanMuscle != null) {
				txtLeanMuscle.Dispose ();
				txtLeanMuscle = null;
			}

			if (txtWeight != null) {
				txtWeight.Dispose ();
				txtWeight = null;
			}
		}
	}
}
