using System;
using UIKit;
using Foundation;
using CoreAnimation;
using CoreGraphics;
using ObjCRuntime;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
namespace Alex.iOS
{
	[Register("CustomPickerView")]
	public partial class CustomPickerView : UIView

	{

		PickerViewModel pickerModel;
		static ProfileType currentProfileType;
		public CustomPickerView(IntPtr handle) : base(handle)
		{
		}
		public static CustomPickerView Create(ProfileType type)
		{

			currentProfileType = type;
			var arr = NSBundle.MainBundle.LoadNib("CustomPickerView", null, null);
			Console.WriteLine(arr);
			//var v = Runtime.GetNSObject<CustomPickerView>(arr.ValueAt(0));
			var v = Runtime.GetNSObject(arr.ValueAt(0)) as CustomPickerView;
			Console.WriteLine(v);

			return v;
		}


		public void SetPickerData(List<string> items)
		{
			pickerModel = new PickerViewModel (items);
			pickerView.Model = pickerModel;
		}


		partial void dismissPicker(UIBarButtonItem sender)
		{
			string type = "SetPickerData";
			if (currentProfileType == ProfileType.Biometric)
			{
				type = "SetBiometricPickerData";
			}
			else if (currentProfileType == ProfileType.Sports)
			{
				type = "SetSportsPickerData";
			}
			else if (currentProfileType == ProfileType.Injury)
			{
				type = "SetInjuryPickerData";
			}
			NSNotificationCenter.DefaultCenter.PostNotificationName(type, null);
			this.RemoveFromSuperview();

		}

		partial void donePressed(UIBarButtonItem sender)
		{
			string type = "SetPickerData";
			if (currentProfileType == ProfileType.Biometric)
			{
				type = "SetBiometricPickerData";
			}
			else if (currentProfileType == ProfileType.Sports)
			{
				type = "SetSportsPickerData";
			}
			else if (currentProfileType == ProfileType.Injury)
			{
				type = "SetInjuryPickerData";
			}
			NSNotificationCenter.DefaultCenter.PostNotificationName(type, NSString.FromObject(pickerModel.SelectedItem));
            this.RemoveFromSuperview();
		}
		//public CustomPickerView()
		//{
		//}
	}

public class PickerViewModel : UIPickerViewModel
{
		
		private List<string> _myItems;
		protected int selectedIndex = 0;

		public PickerViewModel(List<string> items)
		{
			_myItems = items;   
		
		}
		public string SelectedItem
		{
			get { 
				return _myItems[selectedIndex]; 
				}
		}
		public override void Selected(UIPickerView pickerView, nint row, nint component)
		{
			selectedIndex = (int)row;
			//base.Selected(pickerView, row, component);
		}
		public override nint GetRowsInComponent(UIPickerView pickerView, nint component)
		{
			return _myItems.Count;
		}

		public override string GetTitle(UIPickerView pickerView, nint row, nint component)
		{
			return _myItems[(int)row];
		}

		public override nint GetComponentCount(UIPickerView pickerView)
		{

			return 1;
		}
 }
}