// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace Alex.iOS
{
	[Register ("InjuryCell")]
	partial class InjuryCell
	{
		[Outlet]
		UIKit.UIButton btnBodyPart { get; set; }

		[Outlet]
		UIKit.UIButton btnInjuryType { get; set; }

		[Outlet]
		UIKit.UIButton btnRecoveryTime { get; set; }

		[Outlet]
		UIKit.UIImageView imgProfile { get; set; }

		[Outlet]
		UIKit.UILabel lblCity { get; set; }

		[Outlet]
		UIKit.UILabel lblName { get; set; }

		[Outlet]
		UIKit.UILabel lblSliderValue { get; set; }

		[Outlet]
		UIKit.UISlider slider { get; set; }

		[Outlet]
		UIKit.UITextField txtNumberOfInjuries { get; set; }

		[Outlet]
		UIKit.UITextView txtVDesc { get; set; }

		[Action ("InjuryPickerView:")]
		partial void InjuryPickerView (UIKit.UIButton sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (btnBodyPart != null) {
				btnBodyPart.Dispose ();
				btnBodyPart = null;
			}

			if (btnInjuryType != null) {
				btnInjuryType.Dispose ();
				btnInjuryType = null;
			}

			if (btnRecoveryTime != null) {
				btnRecoveryTime.Dispose ();
				btnRecoveryTime = null;
			}

			if (imgProfile != null) {
				imgProfile.Dispose ();
				imgProfile = null;
			}

			if (lblCity != null) {
				lblCity.Dispose ();
				lblCity = null;
			}

			if (lblName != null) {
				lblName.Dispose ();
				lblName = null;
			}

			if (slider != null) {
				slider.Dispose ();
				slider = null;
			}

			if (txtNumberOfInjuries != null) {
				txtNumberOfInjuries.Dispose ();
				txtNumberOfInjuries = null;
			}

			if (txtVDesc != null) {
				txtVDesc.Dispose ();
				txtVDesc = null;
			}

			if (lblSliderValue != null) {
				lblSliderValue.Dispose ();
				lblSliderValue = null;
			}
		}
	}
}
