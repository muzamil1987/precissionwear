// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace Alex.iOS
{
	[Register ("SessionSetupController")]
	partial class SessionSetupController
	{
		[Outlet]
		UIKit.UIBarButtonItem btnBack { get; set; }

		[Outlet]
		UIKit.UIButton btnFieldSize { get; set; }

		[Outlet]
		UIKit.UIButton btnSessionType { get; set; }

		[Outlet]
		UIKit.UIButton btnStartedElapsed { get; set; }

		[Outlet]
		UIKit.UIButton btnStartTime { get; set; }

		[Outlet]
		UIKit.UITableView tblListing { get; set; }

		[Action ("AddSessionPressed:")]
		partial void AddSessionPressed (UIKit.UIButton sender);

		[Action ("BtnBack_Activated:")]
		partial void BtnBack_Activated (UIKit.UIBarButtonItem sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (btnBack != null) {
				btnBack.Dispose ();
				btnBack = null;
			}

			if (btnFieldSize != null) {
				btnFieldSize.Dispose ();
				btnFieldSize = null;
			}

			if (btnSessionType != null) {
				btnSessionType.Dispose ();
				btnSessionType = null;
			}

			if (btnStartedElapsed != null) {
				btnStartedElapsed.Dispose ();
				btnStartedElapsed = null;
			}

			if (btnStartTime != null) {
				btnStartTime.Dispose ();
				btnStartTime = null;
			}

			if (tblListing != null) {
				tblListing.Dispose ();
				tblListing = null;
			}
		}
	}
}
