// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace Alex.iOS
{
	[Register ("DashboardController")]
	partial class DashboardController
	{
		[Outlet]
		UIKit.UIButton btnAcuteChanges { get; set; }

		[Outlet]
		UIKit.UIButton btnChronicTrends { get; set; }

		[Outlet]
		UIKit.UIButton btnCorrelation { get; set; }

		[Outlet]
		UIKit.UIButton btnDashboard { get; set; }

		[Outlet]
		UIKit.UIButton btnPerformance { get; set; }

		[Outlet]
		UIKit.UIButton btnSummaryCard { get; set; }

		[Outlet]
		UIKit.UIView viewAcuteChanges { get; set; }

		[Outlet]
		UIKit.UIView viewChronicTrends { get; set; }

		[Outlet]
		UIKit.UIView viewCorrelation { get; set; }

		[Outlet]
		UIKit.UIView viewDashboard { get; set; }

		[Outlet]
		UIKit.UIView viewPerformance { get; set; }

		[Outlet]
		UIKit.UIView viewSummaryCard { get; set; }

		[Action ("SelectMenuPressed:")]
		partial void SelectMenuPressed (UIKit.UIButton sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (btnDashboard != null) {
				btnDashboard.Dispose ();
				btnDashboard = null;
			}

			if (viewDashboard != null) {
				viewDashboard.Dispose ();
				viewDashboard = null;
			}

			if (btnSummaryCard != null) {
				btnSummaryCard.Dispose ();
				btnSummaryCard = null;
			}

			if (viewSummaryCard != null) {
				viewSummaryCard.Dispose ();
				viewSummaryCard = null;
			}

			if (btnCorrelation != null) {
				btnCorrelation.Dispose ();
				btnCorrelation = null;
			}

			if (viewCorrelation != null) {
				viewCorrelation.Dispose ();
				viewCorrelation = null;
			}

			if (btnAcuteChanges != null) {
				btnAcuteChanges.Dispose ();
				btnAcuteChanges = null;
			}

			if (viewAcuteChanges != null) {
				viewAcuteChanges.Dispose ();
				viewAcuteChanges = null;
			}

			if (btnChronicTrends != null) {
				btnChronicTrends.Dispose ();
				btnChronicTrends = null;
			}

			if (viewChronicTrends != null) {
				viewChronicTrends.Dispose ();
				viewChronicTrends = null;
			}

			if (btnPerformance != null) {
				btnPerformance.Dispose ();
				btnPerformance = null;
			}

			if (viewPerformance != null) {
				viewPerformance.Dispose ();
				viewPerformance = null;
			}
		}
	}
}
