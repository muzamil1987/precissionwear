﻿using System;
using System.Collections.Generic;
using Foundation;
using UIKit;

namespace Alex.iOS
{
	public partial class SummaryCardController : UIViewController
	{
		public SummaryCardController(IntPtr handle) : base(handle)
		{
		}
		public SummaryCardController() : base("SummaryCardController", null)
		{
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			//NavigationController.NavigationBar.SetBackgroundImage(new UIImage(), UIBarMetrics.Default);
			//NavigationController.NavigationBar.ShadowImage = new UIImage();

			string[] tableItems = new string[] { "Vegetables","Vegetables","Vegetables", "Vegetables","Vegetables", "Vegetables","Vegetables", "Vegetables" };

			Dictionary<string, string>[] items = new Dictionary<string, string>[]
			{
				 new Dictionary<string, string> { {"title", "Session Type"},{"subtitle", "WArm Up"},{"image", "sessiondetail_icon1"} },
				 new Dictionary<string, string> { {"title", "Pitch Size"},{"subtitle", "20x40"},{"image", "sessiondetail_icon2"}  }, 
				//new Dictionary<string, string> { {"title", "Player Involved"},{"subtitle", "05"},{"image", "sessiondetail_icon3"}  }, 
				new Dictionary<string, string> { {"title", "Start Time"},{"subtitle", "6-3-2017 12:00 PM"},{"image", "sessiondetail_icon4"}  },
				new Dictionary<string, string> { {"title", "Time Elpsed"},{"subtitle", "00:45:05"},{"image", "sessiondetail_icon5"}  },
			};
			tblListing.Source = new SummaryCardTableSource(items);
			// Perform any additional setup after loading the view, typically from a nib.
		}

		public override void DidReceiveMemoryWarning()
		{
			base.DidReceiveMemoryWarning();
			// Release any cached data, images, etc that aren't in use.
		}
	}


	public class SummaryCardTableSource : UITableViewSource
	{
		Dictionary<string, string>[] TableItems;
		static NSString MyCellId = new NSString("summaryCardCell");

		public SummaryCardTableSource(Dictionary<string, string>[] items)
		{

			TableItems = items;
		}

		public override nint RowsInSection(UITableView tableview, nint section)
		{
			return TableItems.Length;
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{


			var cell = (SummaryCardTableCell)tableView.DequeueReusableCell(MyCellId, indexPath);
			Dictionary<string, string> item = TableItems[indexPath.Row];
			//cell.TextLabel.Text = item;
			cell.SelectionStyle = UITableViewCellSelectionStyle.None;
			//cell.setInitialData(item);
			//cell.SetThumbImage(UIImage.FromFile("29_icon.png"), UIControlState.Normal);
			return cell;
		}
	}
}

