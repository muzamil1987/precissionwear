﻿using System;
using System.Collections.Generic;
using Foundation;
using UIKit;

namespace Alex.iOS
{
	public partial class SessionDetailTableCell : UITableViewCell
	{
		public static readonly NSString Key = new NSString("SessionDetailTableCell");
		public static readonly UINib Nib;

		static SessionDetailTableCell()
		{
			Nib = UINib.FromName("SessionDetailTableCell", NSBundle.MainBundle);
		}

		protected SessionDetailTableCell(IntPtr handle) : base(handle)
		{
			// Note: this .ctor should not contain any initialization logic.
		}

		public void setInitialData(Dictionary<string, string> data)
		{
			lblTitle.Text = data["title"];
			lblDetail.Text = data["subtitle"];
			imgSessionIcon.Image = UIImage.FromBundle(data["image"]);
		}
	}
}
