﻿using System;

using UIKit;
using Foundation;
namespace Alex.iOS
{
	public partial class SportContainerController : UIViewController
	{
		public SportContainerController(IntPtr handle) : base(handle)
		{
		}
		public SportContainerController() : base("SportContainerController", null)
		{
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			// Perform any additional setup after loading the view, typically from a nib.
			string[] tableItems = new string[] { "Vegetables" };
			tblListing.Source = new TableSourceSport(tableItems);
		}

		public override void DidReceiveMemoryWarning()
		{
			base.DidReceiveMemoryWarning();
			// Release any cached data, images, etc that aren't in use.
		}
	}
public class TableSourceSport : UITableViewSource
{
	string[] TableItems;
	static NSString MyCellId = new NSString("sportCell");

	public TableSourceSport(string[] items)
	{

		TableItems = items;
	}

	public override nint RowsInSection(UITableView tableview, nint section)
	{
		return TableItems.Length;
	}

	public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
	{


		var cell = (SportCell)tableView.DequeueReusableCell(MyCellId, indexPath);
		string item = TableItems[indexPath.Row];
		//cell.TextLabel.Text = item;
		cell.SelectionStyle = UITableViewCellSelectionStyle.None;
		cell.setInitialData();
		//cell.SetThumbImage(UIImage.FromFile("29_icon.png"), UIControlState.Normal);
		return cell;
	}
}
}

