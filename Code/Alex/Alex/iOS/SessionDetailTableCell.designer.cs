// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace Alex.iOS
{
	[Register ("SessionDetailTableCell")]
	partial class SessionDetailTableCell
	{
		[Outlet]
		UIKit.UIImageView imgSessionIcon { get; set; }

		[Outlet]
		UIKit.UILabel lblDetail { get; set; }

		[Outlet]
		UIKit.UILabel lblTitle { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (imgSessionIcon != null) {
				imgSessionIcon.Dispose ();
				imgSessionIcon = null;
			}

			if (lblTitle != null) {
				lblTitle.Dispose ();
				lblTitle = null;
			}

			if (lblDetail != null) {
				lblDetail.Dispose ();
				lblDetail = null;
			}
		}
	}
}
