// WARNING
// This file has been generated automatically by Xamarin Studio to
// mirror C# types. Changes in this file made by drag-connecting
// from the UI designer will be synchronized back to C#, but
// more complex manual changes may not transfer correctly.


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface ProfileBiometricController : UIViewController {
	UIView *_biometricContainer;
	UIStackView *_bottomStack;
	UIButton *_btnBiometric;
	UIButton *_btnCancel;
	UIButton *_btnInjury;
	UIButton *_btnNext;
	UIButton *_btnSport;
	UIView *_injuryContainer;
	NSLayoutConstraint *_leadingBiometric;
	NSLayoutConstraint *_leadingInjury;
	NSLayoutConstraint *_leadingSports;
	UIView *_sportContainer;
	UIStackView *_stackMenu;
	UIView *_testView;
	UIView *_viewBiometric;
	UIView *_viewInjury;
	UIView *_viewSports;
}

@property (nonatomic, retain) IBOutlet UIView *biometricContainer;

@property (nonatomic, retain) IBOutlet UIStackView *bottomStack;

@property (nonatomic, retain) IBOutlet UIButton *btnBiometric;

@property (nonatomic, retain) IBOutlet UIButton *btnCancel;

@property (nonatomic, retain) IBOutlet UIButton *btnInjury;

@property (nonatomic, retain) IBOutlet UIButton *btnNext;

@property (nonatomic, retain) IBOutlet UIButton *btnSport;

@property (nonatomic, retain) IBOutlet UIView *injuryContainer;

@property (nonatomic, retain) IBOutlet NSLayoutConstraint *leadingBiometric;

@property (nonatomic, retain) IBOutlet NSLayoutConstraint *leadingInjury;

@property (nonatomic, retain) IBOutlet NSLayoutConstraint *leadingSports;

@property (nonatomic, retain) IBOutlet UIView *sportContainer;

@property (nonatomic, retain) IBOutlet UIStackView *stackMenu;

@property (nonatomic, retain) IBOutlet UIView *testView;

@property (nonatomic, retain) IBOutlet UIView *viewBiometric;

@property (nonatomic, retain) IBOutlet UIView *viewInjury;

@property (nonatomic, retain) IBOutlet UIView *viewSports;

- (IBAction)BiometricPressed:(UIButton *)sender;

- (IBAction)InjuryPressed:(UIButton *)sender;

- (IBAction)NextPressed:(UIButton *)sender;

- (IBAction)SportPressed:(UIButton *)sender;

@end
