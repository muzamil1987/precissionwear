// WARNING
// This file has been generated automatically by Xamarin Studio to
// mirror C# types. Changes in this file made by drag-connecting
// from the UI designer will be synchronized back to C#, but
// more complex manual changes may not transfer correctly.


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface BiometricCell : UITableViewCell {
	UIButton *_btnLowerBody;
	UIButton *_btnUpperBody;
	UIImageView *_imgProfile;
	UILabel *_lblCity;
	UILabel *_lblName;
	UILabel *_lblSliderValue;
	UISlider *_slider;
	UITextField *_txtAge;
	UITextField *_txtHeight;
	UITextField *_txtLeanMuscle;
	UITextField *_txtWeight;
}

@property (nonatomic, retain) IBOutlet UIButton *btnLowerBody;

@property (nonatomic, retain) IBOutlet UIButton *btnUpperBody;

@property (nonatomic, retain) IBOutlet UIImageView *imgProfile;

@property (nonatomic, retain) IBOutlet UILabel *lblCity;

@property (nonatomic, retain) IBOutlet UILabel *lblName;

@property (nonatomic, retain) IBOutlet UILabel *lblSliderValue;

@property (nonatomic, retain) IBOutlet UISlider *slider;

@property (nonatomic, retain) IBOutlet UITextField *txtAge;

@property (nonatomic, retain) IBOutlet UITextField *txtHeight;

@property (nonatomic, retain) IBOutlet UITextField *txtLeanMuscle;

@property (nonatomic, retain) IBOutlet UITextField *txtWeight;

- (IBAction)BiometricPickerView:(UIButton *)sender;

@end
