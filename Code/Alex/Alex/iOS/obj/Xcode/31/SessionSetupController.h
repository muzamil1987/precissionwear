// WARNING
// This file has been generated automatically by Xamarin Studio to
// mirror C# types. Changes in this file made by drag-connecting
// from the UI designer will be synchronized back to C#, but
// more complex manual changes may not transfer correctly.


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface SessionSetupController : UIViewController {
	UIBarButtonItem *_btnBack;
	UIButton *_btnFieldSize;
	UIButton *_btnSessionType;
	UIButton *_btnStartedElapsed;
	UIButton *_btnStartTime;
	UITableView *_tblListing;
}

@property (nonatomic, retain) IBOutlet UIBarButtonItem *btnBack;

@property (nonatomic, retain) IBOutlet UIButton *btnFieldSize;

@property (nonatomic, retain) IBOutlet UIButton *btnSessionType;

@property (nonatomic, retain) IBOutlet UIButton *btnStartedElapsed;

@property (nonatomic, retain) IBOutlet UIButton *btnStartTime;

@property (nonatomic, retain) IBOutlet UITableView *tblListing;

- (IBAction)AddSessionPressed:(UIButton *)sender;

- (IBAction)BtnBack_Activated:(UIBarButtonItem *)sender;

@end
