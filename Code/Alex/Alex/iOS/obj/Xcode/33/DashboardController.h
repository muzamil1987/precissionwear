// WARNING
// This file has been generated automatically by Xamarin Studio to
// mirror C# types. Changes in this file made by drag-connecting
// from the UI designer will be synchronized back to C#, but
// more complex manual changes may not transfer correctly.


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface DashboardController : UIViewController {
}
@property (retain, nonatomic) IBOutlet UIButton *btnDashboard;
@property (retain, nonatomic) IBOutlet UIView *viewDashboard;

@property (retain, nonatomic) IBOutlet UIButton *btnSummaryCard;
@property (retain, nonatomic) IBOutlet UIView *viewSummaryCard;


@property (retain, nonatomic) IBOutlet UIButton *btnCorrelation;
@property (retain, nonatomic) IBOutlet UIView *viewCorrelation;

@property (retain, nonatomic) IBOutlet UIButton *btnAcuteChanges;
@property (retain, nonatomic) IBOutlet UIView *viewAcuteChanges;

@property (retain, nonatomic) IBOutlet UIButton *btnChronicTrends;
@property (retain, nonatomic) IBOutlet UIView *viewChronicTrends;

@property (retain, nonatomic) IBOutlet UIButton *btnPerformance;
@property (retain, nonatomic) IBOutlet UIView *viewPerformance;

- (IBAction)SelectMenuPressed:(UIButton *)sender;

@end
