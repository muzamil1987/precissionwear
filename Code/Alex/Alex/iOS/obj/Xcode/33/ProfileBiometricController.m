// WARNING
// This file has been generated automatically by Xamarin Studio to
// mirror C# types. Changes in this file made by drag-connecting
// from the UI designer will be synchronized back to C#, but
// more complex manual changes may not transfer correctly.


#import "ProfileBiometricController.h"

@implementation ProfileBiometricController

@synthesize biometricContainer = _biometricContainer;
@synthesize bottomStack = _bottomStack;
@synthesize btnBiometric = _btnBiometric;
@synthesize btnCancel = _btnCancel;
@synthesize btnInjury = _btnInjury;
@synthesize btnNext = _btnNext;
@synthesize btnSport = _btnSport;
@synthesize injuryContainer = _injuryContainer;
@synthesize leadingBiometric = _leadingBiometric;
@synthesize leadingInjury = _leadingInjury;
@synthesize leadingSports = _leadingSports;
@synthesize sportContainer = _sportContainer;
@synthesize stackMenu = _stackMenu;
@synthesize testView = _testView;
@synthesize viewBiometric = _viewBiometric;
@synthesize viewInjury = _viewInjury;
@synthesize viewSports = _viewSports;

- (IBAction)BiometricPressed:(UIButton *)sender {
}

- (IBAction)InjuryPressed:(UIButton *)sender {
}

- (IBAction)NextPressed:(UIButton *)sender {
}

- (IBAction)SportPressed:(UIButton *)sender {
}

@end
