// WARNING
// This file has been generated automatically by Xamarin Studio to
// mirror C# types. Changes in this file made by drag-connecting
// from the UI designer will be synchronized back to C#, but
// more complex manual changes may not transfer correctly.


#import "InjuryCell.h"

@implementation InjuryCell

@synthesize btnBodyPart = _btnBodyPart;
@synthesize btnInjuryType = _btnInjuryType;
@synthesize btnRecoveryTime = _btnRecoveryTime;
@synthesize imgProfile = _imgProfile;
@synthesize lblCity = _lblCity;
@synthesize lblName = _lblName;
@synthesize lblSliderValue = _lblSliderValue;
@synthesize slider = _slider;
@synthesize txtNumberOfInjuries = _txtNumberOfInjuries;
@synthesize txtVDesc = _txtVDesc;

- (IBAction)InjuryPickerView:(UIButton *)sender {
}

@end
