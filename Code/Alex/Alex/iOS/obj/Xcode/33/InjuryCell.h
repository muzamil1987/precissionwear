// WARNING
// This file has been generated automatically by Xamarin Studio to
// mirror C# types. Changes in this file made by drag-connecting
// from the UI designer will be synchronized back to C#, but
// more complex manual changes may not transfer correctly.


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface InjuryCell : UITableViewCell {
	UIButton *_btnBodyPart;
	UIButton *_btnInjuryType;
	UIButton *_btnRecoveryTime;
	UIImageView *_imgProfile;
	UILabel *_lblCity;
	UILabel *_lblName;
	UILabel *_lblSliderValue;
	UISlider *_slider;
	UITextField *_txtNumberOfInjuries;
	UITextView *_txtVDesc;
}

@property (nonatomic, retain) IBOutlet UIButton *btnBodyPart;

@property (nonatomic, retain) IBOutlet UIButton *btnInjuryType;

@property (nonatomic, retain) IBOutlet UIButton *btnRecoveryTime;

@property (nonatomic, retain) IBOutlet UIImageView *imgProfile;

@property (nonatomic, retain) IBOutlet UILabel *lblCity;

@property (nonatomic, retain) IBOutlet UILabel *lblName;

@property (nonatomic, retain) IBOutlet UILabel *lblSliderValue;

@property (nonatomic, retain) IBOutlet UISlider *slider;

@property (nonatomic, retain) IBOutlet UITextField *txtNumberOfInjuries;

@property (nonatomic, retain) IBOutlet UITextView *txtVDesc;

- (IBAction)InjuryPickerView:(UIButton *)sender;

@end
