// WARNING
// This file has been generated automatically by Xamarin Studio to
// mirror C# types. Changes in this file made by drag-connecting
// from the UI designer will be synchronized back to C#, but
// more complex manual changes may not transfer correctly.


#import "DashboardController.h"

@implementation DashboardController

- (void)dealloc {
    [_btnDashboard release];
    [_viewDashboard release];
    [_btnSummaryCard release];
    [_viewSummaryCard release];
    [_btnCorrelation release];
    [_viewCorrelation release];
    [_btnAcuteChanges release];
    [_viewAcuteChanges release];
    [_btnChronicTrends release];
    [_viewChronicTrends release];
    [_btnPerformance release];
    [_viewPerformance release];
    [super dealloc];
}
- (IBAction)SelectMenuPressed:(UIButton *)sender {
}
@end
