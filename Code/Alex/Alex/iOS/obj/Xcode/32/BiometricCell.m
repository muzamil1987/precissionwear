// WARNING
// This file has been generated automatically by Xamarin Studio to
// mirror C# types. Changes in this file made by drag-connecting
// from the UI designer will be synchronized back to C#, but
// more complex manual changes may not transfer correctly.


#import "BiometricCell.h"

@implementation BiometricCell

@synthesize btnLowerBody = _btnLowerBody;
@synthesize btnUpperBody = _btnUpperBody;
@synthesize imgProfile = _imgProfile;
@synthesize lblCity = _lblCity;
@synthesize lblName = _lblName;
@synthesize lblSliderValue = _lblSliderValue;
@synthesize slider = _slider;
@synthesize txtAge = _txtAge;
@synthesize txtHeight = _txtHeight;
@synthesize txtLeanMuscle = _txtLeanMuscle;
@synthesize txtWeight = _txtWeight;

- (IBAction)BiometricPickerView:(UIButton *)sender {
}

@end
