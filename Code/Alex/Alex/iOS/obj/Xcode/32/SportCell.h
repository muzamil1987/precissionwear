// WARNING
// This file has been generated automatically by Xamarin Studio to
// mirror C# types. Changes in this file made by drag-connecting
// from the UI designer will be synchronized back to C#, but
// more complex manual changes may not transfer correctly.


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface SportCell : UITableViewCell {
	UIButton *_btnLevelSport;
	UIButton *_btnLevelTeam;
	UIButton *_btnSportType;
	UIButton *_btnYearsPlaying;
	UITextField *_txtPrimaryPosition;
	UITextField *_txtSecondaryPosition;
}

@property (nonatomic, retain) IBOutlet UIButton *btnLevelSport;

@property (nonatomic, retain) IBOutlet UIButton *btnLevelTeam;

@property (nonatomic, retain) IBOutlet UIButton *btnSportType;

@property (nonatomic, retain) IBOutlet UIButton *btnYearsPlaying;

@property (nonatomic, retain) IBOutlet UITextField *txtPrimaryPosition;

@property (nonatomic, retain) IBOutlet UITextField *txtSecondaryPosition;

- (IBAction)SportPickerView:(UIButton *)sender;

@end
