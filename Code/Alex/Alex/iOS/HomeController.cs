﻿ using System;

using UIKit;
using Foundation;
namespace Alex.iOS
{
	public partial class HomeController : UIViewController
	{

		public HomeController(IntPtr handle) : base(handle)
		{
		}
		public HomeController() : base("HomeController", null)
		{
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			// Perform any additional setup after loading the view, typically from a nib.

			//tblListing.RegisterNibForCellReuse(UINib.FromName("homeDataCell", NSBundle.MainBundle), MyCellId)
			         
			string[] tableItems = new string[] { "Vegetables" };
			tblListing.Source = new TableSourceHome(tableItems);
		}

		public override void DidReceiveMemoryWarning()
		{
			base.DidReceiveMemoryWarning();
			// Release any cached data, images, etc that aren't in use.
		}
	}
}
public class TableSourceHome : UITableViewSource
{
	string[] TableItems;
	static NSString MyCellId = new NSString("homeDataCell");

	public TableSourceHome(string[] items)
	{

		TableItems = items;
	}

	public override nint RowsInSection(UITableView tableview, nint section)
	{
		return TableItems.Length;
	}

	public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
	{


		var cell = (UITableViewCell)tableView.DequeueReusableCell(MyCellId, indexPath);
		string item = TableItems[indexPath.Row];
		//cell.TextLabel.Text = item;
		cell.SelectionStyle = UITableViewCellSelectionStyle.None;
		//cell.setInitialData();
		//cell.SetThumbImage(UIImage.FromFile("29_icon.png"), UIControlState.Normal);
		return cell;
	}
}

