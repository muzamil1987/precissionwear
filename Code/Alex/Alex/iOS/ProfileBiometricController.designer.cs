// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace Alex.iOS
{
	[Register ("ProfileBiometricController")]
	partial class ProfileBiometricController
	{
		[Outlet]
		UIKit.UIView biometricContainer { get; set; }

		[Outlet]
		UIKit.UIStackView bottomStack { get; set; }

		[Outlet]
		UIKit.UIButton btnBiometric { get; set; }

		[Outlet]
		UIKit.UIButton btnCancel { get; set; }

		[Outlet]
		UIKit.UIButton btnInjury { get; set; }

		[Outlet]
		UIKit.UIButton btnNext { get; set; }

		[Outlet]
		UIKit.UIButton btnSport { get; set; }

		[Outlet]
		UIKit.UIView injuryContainer { get; set; }

		[Outlet]
		UIKit.NSLayoutConstraint leadingBiometric { get; set; }

		[Outlet]
		UIKit.NSLayoutConstraint leadingInjury { get; set; }

		[Outlet]
		UIKit.NSLayoutConstraint leadingSports { get; set; }

		[Outlet]
		UIKit.UIView sportContainer { get; set; }

		[Outlet]
		UIKit.UIStackView stackMenu { get; set; }

		[Outlet]
		UIKit.UIView testView { get; set; }

		[Outlet]
		UIKit.UIView viewBiometric { get; set; }

		[Outlet]
		UIKit.UIView viewInjury { get; set; }

		[Outlet]
		UIKit.UIView viewSports { get; set; }

		[Action ("BiometricPressed:")]
		partial void BiometricPressed (UIKit.UIButton sender);

		[Action ("InjuryPressed:")]
		partial void InjuryPressed (UIKit.UIButton sender);

		[Action ("NextPressed:")]
		partial void NextPressed (UIKit.UIButton sender);

		[Action ("SportPressed:")]
		partial void SportPressed (UIKit.UIButton sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (biometricContainer != null) {
				biometricContainer.Dispose ();
				biometricContainer = null;
			}

			if (bottomStack != null) {
				bottomStack.Dispose ();
				bottomStack = null;
			}

			if (btnBiometric != null) {
				btnBiometric.Dispose ();
				btnBiometric = null;
			}

			if (btnCancel != null) {
				btnCancel.Dispose ();
				btnCancel = null;
			}

			if (btnInjury != null) {
				btnInjury.Dispose ();
				btnInjury = null;
			}

			if (btnNext != null) {
				btnNext.Dispose ();
				btnNext = null;
			}

			if (btnSport != null) {
				btnSport.Dispose ();
				btnSport = null;
			}

			if (injuryContainer != null) {
				injuryContainer.Dispose ();
				injuryContainer = null;
			}

			if (leadingBiometric != null) {
				leadingBiometric.Dispose ();
				leadingBiometric = null;
			}

			if (leadingInjury != null) {
				leadingInjury.Dispose ();
				leadingInjury = null;
			}

			if (leadingSports != null) {
				leadingSports.Dispose ();
				leadingSports = null;
			}

			if (sportContainer != null) {
				sportContainer.Dispose ();
				sportContainer = null;
			}

			if (stackMenu != null) {
				stackMenu.Dispose ();
				stackMenu = null;
			}

			if (testView != null) {
				testView.Dispose ();
				testView = null;
			}

			if (viewBiometric != null) {
				viewBiometric.Dispose ();
				viewBiometric = null;
			}

			if (viewInjury != null) {
				viewInjury.Dispose ();
				viewInjury = null;
			}

			if (viewSports != null) {
				viewSports.Dispose ();
				viewSports = null;
			}
		}
	}
}
