﻿using System;

using UIKit;

namespace Alex.iOS
{
	public enum DashboardType : int
	{
		Dashboard = 1,
		SummaryCard = 2,
		Correlation = 3,
		AcuteChanges = 4,
		ChronicTrends = 5,
		Performance = 6
	}

	public partial class DashboardController : UIViewController
	{
		static nfloat kFONT_WIDTH_FACTOR = UIScreen.MainScreen.Bounds.Width / 414;
		DashboardType currentType;
		public DashboardController(IntPtr handle) : base(handle)
		{
		}
		public DashboardController() : base("DashboardController", null)
		{
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			NavigationController.NavigationBar.SetBackgroundImage(new UIImage(), UIBarMetrics.Default);
			NavigationController.NavigationBar.ShadowImage = new UIImage();

			btnDashboard.Selected = false;
			btnSummaryCard.Selected = false;
			btnCorrelation.Selected = false;
			btnAcuteChanges.Selected = false;
			btnChronicTrends.Selected = false;
			btnPerformance.Selected = false;

			viewDashboard.Hidden = true;
			viewSummaryCard.Hidden = true;
			viewCorrelation.Hidden = true;
			viewAcuteChanges.Hidden = true;
			viewChronicTrends.Hidden = true;
			viewPerformance.Hidden = true;

			btnDashboard.TitleLabel.Font = UIFont.FromName("Arial", kFONT_WIDTH_FACTOR * 17f);
			btnSummaryCard.TitleLabel.Font = UIFont.FromName("Arial", kFONT_WIDTH_FACTOR * 17f);
			btnCorrelation.TitleLabel.Font = UIFont.FromName("Arial", kFONT_WIDTH_FACTOR * 17f);
			btnAcuteChanges.TitleLabel.Font = UIFont.FromName("Arial", kFONT_WIDTH_FACTOR * 17f);
			btnChronicTrends.TitleLabel.Font = UIFont.FromName("Arial", kFONT_WIDTH_FACTOR * 17f);
			btnPerformance.TitleLabel.Font = UIFont.FromName("Arial", kFONT_WIDTH_FACTOR * 17f);

			currentType = DashboardType.Dashboard;
			btnDashboard.Selected = true;
			viewDashboard.Hidden = false;
			// Perform any additional setup after loading the view, typically from a nib.
		}

		partial void SelectMenuPressed(UIButton sender)
		{

			btnDashboard.Selected = false;
			btnSummaryCard.Selected = false;
			btnCorrelation.Selected = false;
			btnAcuteChanges.Selected = false;
			btnChronicTrends.Selected = false;
			btnPerformance.Selected = false;

			viewDashboard.Hidden = true;
			viewSummaryCard.Hidden = true;
			viewCorrelation.Hidden = true;
			viewAcuteChanges.Hidden = true;
			viewChronicTrends.Hidden = true;
			viewPerformance.Hidden = true;

			currentType = (DashboardType)Enum.ToObject(typeof(DashboardType), sender.Tag);

			switch (currentType)
			{
				case (DashboardType.Dashboard):
					{
						btnDashboard.Selected = true;
						viewDashboard.Hidden = false;
						break;
					}
				case (DashboardType.SummaryCard):
					{
						btnSummaryCard.Selected = true;
						viewSummaryCard.Hidden = false;
						break;
					}
				case (DashboardType.Correlation):
					{
						btnCorrelation.Selected = true;
						viewCorrelation.Hidden = false;
						break;
					}
				case (DashboardType.AcuteChanges):
					{
						btnAcuteChanges.Selected = true;
						viewAcuteChanges.Hidden = false;
						break;
					}
				case (DashboardType.ChronicTrends):
					{
						btnChronicTrends.Selected = true;
						viewChronicTrends.Hidden = false;
						break;
					}
				case (DashboardType.Performance):
					{
						btnPerformance.Selected = true;
						viewPerformance.Hidden = false;
						break;
					}

			}

		}

		public override void DidReceiveMemoryWarning()
		{
			base.DidReceiveMemoryWarning();
			// Release any cached data, images, etc that aren't in use.
		}
	}
}

