﻿using System;

using Foundation;
using UIKit;

namespace Alex.iOS
{
	public partial class SummaryCardTableCell : UITableViewCell
	{
		public static readonly NSString Key = new NSString("SummaryCardTableCell");
		public static readonly UINib Nib;

		static SummaryCardTableCell()
		{
			Nib = UINib.FromName("SummaryCardTableCell", NSBundle.MainBundle);
		}

		protected SummaryCardTableCell(IntPtr handle) : base(handle)
		{
			// Note: this .ctor should not contain any initialization logic.
		}
	}
}
