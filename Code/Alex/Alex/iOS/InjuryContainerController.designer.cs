// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace Alex.iOS
{
	[Register ("InjuryContainerController")]
	partial class InjuryContainerController
	{
		[Outlet]
		UIKit.UITableView tblListing { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (tblListing != null) {
				tblListing.Dispose ();
				tblListing = null;
			}
		}
	}
}
