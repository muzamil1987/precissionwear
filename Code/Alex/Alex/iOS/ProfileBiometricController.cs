﻿using System;

using UIKit;
using Foundation;
using CoreAnimation;
using CoreGraphics;
using ObjCRuntime;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
namespace Alex.iOS
{

	public enum PickerType : int
	{
		LowerBodyStrength = 1,
		UpperBodyStrength = 2,
		TypeOfSport = 3,
		YearsPlayingSport = 4,
		LevelAtSport = 5,
		LevelAtTeam = 6,
		TypeOfInjury = 7,
		BodyPartOfInjury = 8,
		RecoveryTypeOfInjury = 9

	}

	public enum ProfileType
{
	Biometric = 1,
	Sports = 2,
	Injury = 3
}


	public partial class ProfileBiometricController : UIViewController
	{
		NSObject notificationToken; 
		static nfloat kFONT_WIDTH_FACTOR = UIScreen.MainScreen.Bounds.Width / 414;
		ProfileType currentType;
		CustomPickerView picker;


	//	private readonly IList<string> colors = new List<string>
	//{
	//"Blue",
	//"Green",
	//"Red",
	//"Purple",
	//"Yellow"  };
		partial void NextPressed(UIButton sender)
		{
			//Console.WriteLine("NEXT");
			//btnBiometric.Selected = false;
			//btnSport.Selected = false;
			//btnInjury.Selected = false;

			//viewBiometric.Hidden = true;
			//viewSports.Hidden = true;
			//viewInjury.Hidden = true;

			switch (currentType)
			{
				case (ProfileType.Biometric):
					{
						//btnNext.SetTitle("NEXT", UIControlState.Normal);
						//btnSport.Selected = true;
						//viewSports.Hidden = false;
						////sportContainer.Hidden = false;
						//currentType = ProfileType.Sports;
						SportPressed(null);
						break;
					}
				case (ProfileType.Sports):
					{
						//btnNext.SetTitle("SAVE", UIControlState.Normal);
						//btnInjury.Selected = true;
						//viewInjury.Hidden = false;
						////injuryContainer.Hidden = false;
						//currentType = ProfileType.Injury;
						InjuryPressed(null);
						break;
					}
				case (ProfileType.Injury):
					{
						
						//btnNext.SetTitle("NEXT", UIControlState.Normal);
						//btnBiometric.Selected = true;
						//viewBiometric.Hidden = false;
						////biometricContainer.Hidden = false;
						//currentType = ProfileType.Biometric;
						gotoHomeController();
						break;
					}
			
			}

			//			throw new NotImplementedException();
		}


		public void gotoHomeController()
		{
			UINavigationController controller = this.Storyboard.InstantiateViewController("HomeNavigationController") as UINavigationController;
			UIApplication.SharedApplication.Windows[0].RootViewController = controller;
		}

		public ProfileBiometricController(IntPtr handle) : base(handle)
		{
		}


		public ProfileBiometricController() : base("ProfileBiometricController", null)
		{
		}


		public void showPickerView(NSNotification notification)
		{
			List<string> items = null;
			NSNumber num = (NSNumber)notification.Object;
			int numInt = (int)num;
			PickerType type = (PickerType)numInt;

			if (type == PickerType.LowerBodyStrength)
			{
				items = new List<string>(){"less than bodyweight","1.5x Bodyweight", "2x bodyweight","more than 2x bodyweight"}; 
			}
			else if (type == PickerType.UpperBodyStrength)
			{
				items = new List<string>(){"less than bodyweight","1.5x Bodyweight","2x bodyweight","more than 2x bodyweight"}; 
			}
			else if (type == PickerType.TypeOfSport)
			{
				items = new List<string>(){"Sport Type 1","Sport Type 2","Sport Type 3","Sport Type 4","Sport Type 5"}; 
			}
			else if (type == PickerType.YearsPlayingSport)
			{
				items = new List<string>(){"Year 1","Year 2","Year 3","Year 4","Year 5","Year 6"}; 
			}
			else if (type == PickerType.LevelAtSport)
			{
				items = new List<string>(){"Weekend", "Amateur", "Semi-Pro", "Olympic", "Professional", "Masters"}; 
			}
			else if (type == PickerType.LevelAtTeam)
			{
				items = new List<string>(){"Level 1","Level 2","Level 3","Level 4","Level 5"}; 
			}
			else if (type == PickerType.TypeOfInjury)
			{
				items = new List<string>(){"Muscular", "Skeletal"}; 
			}
			else if (type == PickerType.BodyPartOfInjury)
			{
				items = new List<string>(){"Body Part 1","Body Part 2","Body Part 3","Body Part 4","Body Part 5"}; 
			}
			else if (type == PickerType.RecoveryTypeOfInjury)
			{
				items = new List<string>(){"Recovery 1","Recovery 2","Recovery 3","Recovery 4","Recovery 5"}; 
			}
			picker = CustomPickerView.Create(currentType);
			Console.WriteLine(picker);
        	picker.Frame = View.Frame;
        	View.AddSubview (picker);
			picker.SetPickerData(items);
		}




		public override void ViewDidLoad()
		{
			
			base.ViewDidLoad();
			//NSNotificationCenter.DefaultCenter.AddObserver("as",ChangeLeftSide);

			this.View.SetNeedsLayout();
			this.View.LayoutIfNeeded();
			//NSNotificationCenter.DefaultCenter.PostNotificationName("populate",???);
			//NSNotificationCenter.DefaultCenter.PostNotificationName((NSString)"showPickerView", null);
			NSNotificationCenter.DefaultCenter.RemoveObserver(this);
			NSNotificationCenter.DefaultCenter.AddObserver ((NSString)"showPickerView", showPickerView);
			//NSNotificationCenter.DefaultCenter.AddObserver ((NSString)"SetPickerData", SetPickerData);


			NavigationController.NavigationBar.SetBackgroundImage(new UIImage(), UIBarMetrics.Default);
			NavigationController.NavigationBar.ShadowImage = new UIImage ();

			currentType = ProfileType.Biometric;

			//btnSport.UserInteractionEnabled = false;
			//btnInjury.UserInteractionEnabled = false;
			//btnBiometric.UserInteractionEnabled = false;

			btnBiometric.Selected = true;
			viewBiometric.Hidden = false;
			viewSports.Hidden = true;
			viewInjury.Hidden = true;

			//biometricContainer.Hidden = false;
			//sportContainer.Hidden = true;
			//injuryContainer.Hidden = true;


			btnBiometric.TitleLabel.Font = UIFont.FromName("Arial",kFONT_WIDTH_FACTOR * 17f);
			btnSport.TitleLabel.Font = UIFont.FromName("Arial",kFONT_WIDTH_FACTOR * 17f);
			btnInjury.TitleLabel.Font = UIFont.FromName("Arial",kFONT_WIDTH_FACTOR * 17f);
			btnNext.TitleLabel.Font = UIFont.FromName("Arial",kFONT_WIDTH_FACTOR * 20f);
			btnCancel.TitleLabel.Font = UIFont.FromName("Arial",kFONT_WIDTH_FACTOR * 20f);
			//btnSport.Selected = true;
			//btnInjury.Selected = true;
			// Perform any additional setup after loading the view, typically from a nib.

			Console.WriteLine(View.Frame);
			leadingBiometric.Constant = 0;
			leadingSports.Constant = View.Frame.Width;
			leadingInjury.Constant = View.Frame.Width * 2;
			biometricContainer.Frame = new CGRect(0, stackMenu.Frame.Bottom, View.Frame.Width, bottomStack.Frame.Top - stackMenu.Frame.Bottom);
			sportContainer.Frame = new CGRect(0, stackMenu.Frame.Bottom, View.Frame.Width, bottomStack.Frame.Top - stackMenu.Frame.Bottom);
			injuryContainer.Frame = new CGRect(0, stackMenu.Frame.Bottom, View.Frame.Width, bottomStack.Frame.Top - stackMenu.Frame.Bottom);
			//viewBiometric.BackgroundColor = UIColor.Orange;
		}



		partial void BiometricPressed(UIButton sender)
		{

			btnBiometric.Selected = true;
			btnSport.Selected = false;
			btnInjury.Selected = false;

			viewBiometric.Hidden = false;
			viewSports.Hidden = true;
			viewInjury.Hidden = true;

			btnNext.SetTitle("NEXT", UIControlState.Normal);
			currentType = ProfileType.Biometric;
			UIView.Animate(0.3f, 0, UIViewAnimationOptions.CurveEaseInOut, (() => {
				
				leadingBiometric.Constant = 0;
				leadingSports.Constant = View.Frame.Width;
				leadingInjury.Constant = View.Frame.Width * 2;
                this.View.LayoutIfNeeded();
				currentType = ProfileType.Biometric;
		}), null);

		}

		partial void SportPressed(UIButton sender)
		{
			
           btnBiometric.Selected = false;
			btnSport.Selected = true;
			btnInjury.Selected = false;

			viewBiometric.Hidden = true;
			viewSports.Hidden = false;
			viewInjury.Hidden = true;
			currentType = ProfileType.Sports;
			btnNext.SetTitle("NEXT", UIControlState.Normal);

			UIView.Animate(0.3f, 0, UIViewAnimationOptions.CurveEaseInOut, (() => {
				
				leadingBiometric.Constant = -View.Frame.Width;
				leadingSports.Constant = 0;
				leadingInjury.Constant = View.Frame.Width;
				this.View.LayoutIfNeeded();
				currentType = ProfileType.Sports;
			}), null);

		}

		partial void InjuryPressed(UIButton sender)
		{
           btnBiometric.Selected = false;
			btnSport.Selected = false;
			btnInjury.Selected = true;

			viewBiometric.Hidden = true;
			viewSports.Hidden = true;
			viewInjury.Hidden = false;
			currentType = ProfileType.Injury;
			btnNext.SetTitle("SAVE", UIControlState.Normal);

			UIView.Animate(0.3f, 0, UIViewAnimationOptions.CurveEaseInOut, (() => {
				
				leadingBiometric.Constant = -View.Frame.Width * 2;
				leadingSports.Constant = -View.Frame.Width ;
				leadingInjury.Constant = 0;
                this.View.LayoutIfNeeded();
				currentType = ProfileType.Injury;
		}), null);
		}

		public override void DidReceiveMemoryWarning()
		{
			base.DidReceiveMemoryWarning();
			// Release any cached data, images, etc that aren't in use.
		}
	}
}

