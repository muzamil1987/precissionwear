// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace Alex.iOS
{
	partial class CustomPickerView
	{
		[Outlet]
		UIKit.UIPickerView pickerView { get; set; }

		[Action ("dismissPicker:")]
		partial void dismissPicker (UIKit.UIBarButtonItem sender);

		[Action ("donePressed:")]
		partial void donePressed (UIKit.UIBarButtonItem sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (pickerView != null) {
				pickerView.Dispose ();
				pickerView = null;
			}
		}
	}
}
