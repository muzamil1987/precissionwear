// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace Alex.iOS
{
	[Register ("SportCell")]
	partial class SportCell
	{
		[Outlet]
		UIKit.UIButton btnLevelSport { get; set; }

		[Outlet]
		UIKit.UIButton btnLevelTeam { get; set; }

		[Outlet]
		UIKit.UIButton btnSportType { get; set; }

		[Outlet]
		UIKit.UIButton btnYearsPlaying { get; set; }

		[Outlet]
		UIKit.UITextField txtPrimaryPosition { get; set; }

		[Outlet]
		UIKit.UITextField txtSecondaryPosition { get; set; }

		[Action ("SportPickerView:")]
		partial void SportPickerView (UIKit.UIButton sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (btnLevelSport != null) {
				btnLevelSport.Dispose ();
				btnLevelSport = null;
			}

			if (btnLevelTeam != null) {
				btnLevelTeam.Dispose ();
				btnLevelTeam = null;
			}

			if (btnSportType != null) {
				btnSportType.Dispose ();
				btnSportType = null;
			}

			if (btnYearsPlaying != null) {
				btnYearsPlaying.Dispose ();
				btnYearsPlaying = null;
			}

			if (txtPrimaryPosition != null) {
				txtPrimaryPosition.Dispose ();
				txtPrimaryPosition = null;
			}

			if (txtSecondaryPosition != null) {
				txtSecondaryPosition.Dispose ();
				txtSecondaryPosition = null;
			}
		}
	}
}
