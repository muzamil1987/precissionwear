﻿using System;
using CoreGraphics;
using Foundation;
using UIKit;

namespace Alex.iOS
{
	public partial class InjuryCell : UITableViewCell
	{
		PickerType selectedPickerType;
		public static readonly NSString Key = new NSString("InjuryCell");
		public static readonly UINib Nib;

		static InjuryCell()
		{
			Nib = UINib.FromName("InjuryCell", NSBundle.MainBundle);
		}

		protected InjuryCell(IntPtr handle) : base(handle)
		{
			// Note: this .ctor should not contain any initialization logic.
		}
		public override void AwakeFromNib()
		{
			base.AwakeFromNib();
			nfloat kFONT_WIDTH_FACTOR = UIScreen.MainScreen.Bounds.Width / 414;
			txtNumberOfInjuries.Font = UIFont.FromName("Arial",kFONT_WIDTH_FACTOR * 12f);
			txtNumberOfInjuries.Font = UIFont.FromName("Arial",kFONT_WIDTH_FACTOR * 12f);;
			btnBodyPart.TitleLabel.Font = UIFont.FromName("Arial",kFONT_WIDTH_FACTOR * 12f);

			btnInjuryType.TitleLabel.Font = UIFont.FromName("Arial",kFONT_WIDTH_FACTOR * 12f);
			btnRecoveryTime.TitleLabel.Font = UIFont.FromName("Arial",kFONT_WIDTH_FACTOR * 12f);
			btnBodyPart.TitleLabel.Font = UIFont.FromName("Arial",kFONT_WIDTH_FACTOR * 12f);

			btnInjuryType.ImageEdgeInsets = new UIEdgeInsets(0, btnInjuryType.Frame.Size.Width - 40, 0,0);
			btnRecoveryTime.ImageEdgeInsets = new UIEdgeInsets(0, btnRecoveryTime.Frame.Size.Width - 40, 0,0);
			btnBodyPart.ImageEdgeInsets = new UIEdgeInsets(0, btnBodyPart.Frame.Size.Width - 40, 0,0);

			NSNotificationCenter.DefaultCenter.RemoveObserver(this);
			NSNotificationCenter.DefaultCenter.AddObserver ((NSString)"SetInjuryPickerData", SetPickerData);


			slider.MinValue = 0;
			slider.MaxValue = 4;
			slider.Value = 2;
			lblSliderValue.Text = "2";
			CGRect trackRect = slider.TrackRectForBounds(slider.Bounds);
			CGRect thumbRect = slider.ThumbRectForBounds(slider.Bounds, trackRect, slider.Value);

//lblCurrentValue.Center = new CGPoint(thumbRect.Width / 2, lblCurrentValue.Center.Y);


			slider.ValueChanged += HandleValueChanged;

		}
		void HandleValueChanged(object sender, EventArgs e)
		{   // display the value in a label
			//label.Text = slider.Value.ToString();

			CGRect trackRect = slider.TrackRectForBounds(slider.Bounds);
			CGRect thumbRect = slider.ThumbRectForBounds(slider.Bounds, trackRect, slider.Value);

			//lblCurrentValue.Center = new CGPoint(slider.Value * slider.Bounds.Size.Width, 40);

			lblSliderValue.Text = Convert.ToString((int)slider.Value);

			//lblCurrentValue.Center = new CGPoint(thumbRect.Width / 2, lblCurrentValue.Center.Y);
			//CGRect trackRect = [self.slider trackRectForBounds: self.slider.bounds];
			//CGRect thumbRect = [self.slider thumbRectForBounds: self.slider.bounds

			//			   trackRect: trackRect

			//				   value: self.slider.value];
			//throw new NotImplementedException();
			//    yourLabel.center = CGPointMake(thumbRect.origin.x + self.slider.frame.origin.x,  self.slider.frame.origin.y - 20);		}

		}
		public void setInitialData()
		{
			slider.SetThumbImage(UIImage.FromBundle("circle"),UIControlState.Normal);
			UIView heightPaddingView = new UIView();
			heightPaddingView.Frame = new CoreGraphics.CGRect(0, 0, 10, txtNumberOfInjuries.Frame.Size.Height);
			txtNumberOfInjuries.LeftView = heightPaddingView;
			txtNumberOfInjuries.LeftViewMode = UITextFieldViewMode.Always;
		}


		public void SetPickerData(NSNotification notification)
		{
			NSNotificationCenter.DefaultCenter.RemoveObserver(this);
			if (notification.Object == null)
			{
				return;
			}
			Console.Write("PICKER DATA");
			NSString title = (NSString)notification.Object;
			if (selectedPickerType == PickerType.TypeOfInjury)
			{
				btnInjuryType.SetTitle(title, UIControlState.Normal);
			}
			else if (selectedPickerType == PickerType.BodyPartOfInjury)
			{
				btnBodyPart.SetTitle(title, UIControlState.Normal);
			}
			else if (selectedPickerType == PickerType.RecoveryTypeOfInjury)
			{
				btnRecoveryTime.SetTitle(title, UIControlState.Normal);
			}

		}
		partial void InjuryPickerView(UIButton sender)
			{
				int tag = (int)sender.Tag;
				selectedPickerType = (PickerType)tag;
				NSNotificationCenter.DefaultCenter.PostNotificationName((NSString)"showPickerView", NSNumber.FromNInt(sender.Tag));
			}
	}
}
