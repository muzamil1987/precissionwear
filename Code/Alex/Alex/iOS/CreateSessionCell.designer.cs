// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace Alex.iOS
{
	[Register ("CreateSessionCell")]
	partial class CreateSessionCell
	{
		[Outlet]
		UIKit.UIButton btnFieldSize { get; set; }

		[Outlet]
		UIKit.UIButton btnSessionType { get; set; }

		[Outlet]
		UIKit.UIButton btnStartedElapsed { get; set; }

		[Outlet]
		UIKit.UIButton btnStartTime { get; set; }

		[Outlet]
		UIKit.UITextField txtSessionName { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (btnStartTime != null) {
				btnStartTime.Dispose ();
				btnStartTime = null;
			}

			if (btnStartedElapsed != null) {
				btnStartedElapsed.Dispose ();
				btnStartedElapsed = null;
			}

			if (btnFieldSize != null) {
				btnFieldSize.Dispose ();
				btnFieldSize = null;
			}

			if (btnSessionType != null) {
				btnSessionType.Dispose ();
				btnSessionType = null;
			}

			if (txtSessionName != null) {
				txtSessionName.Dispose ();
				txtSessionName = null;
			}
		}
	}
}
