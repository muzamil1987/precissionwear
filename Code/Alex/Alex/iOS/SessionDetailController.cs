﻿using System;

using UIKit;
using Foundation;
using System.Collections.Generic;

namespace Alex.iOS
{
	public partial class SessionDetailController : UIViewController
	{
		public SessionDetailController(IntPtr handle) : base(handle)
		{
		}
		public SessionDetailController() : base("SessionDetailController", null)
		{
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();


			string[] tableItems = new string[] { "Vegetables" };

			Dictionary<string, string>[] items = new Dictionary<string, string>[] 
			{ 
 			    new Dictionary<string, string> { {"title", "Session Type"},{"subtitle", "WArm Up"},{"image", "sessiondetail_icon1"} }, 
 			    new Dictionary<string, string> { {"title", "Pitch Size"},{"subtitle", "20x40"},{"image", "sessiondetail_icon2"}  }, 
				//new Dictionary<string, string> { {"title", "Player Involved"},{"subtitle", "05"},{"image", "sessiondetail_icon3"}  }, 
				new Dictionary<string, string> { {"title", "Start Time"},{"subtitle", "6-3-2017 12:00 PM"},{"image", "sessiondetail_icon4"}  }, 
				new Dictionary<string, string> { {"title", "Time Elpsed"},{"subtitle", "00:45:05"},{"image", "sessiondetail_icon5"}  },
			};
			tblListing.Source = new SessionDetailTableSource(items);
			// Perform any additional setup after loading the view, typically from a nib.
		}

		partial void backPressed(UIBarButtonItem sender)
		{
            this.NavigationController.PopViewController(true);
		}

		public override void DidReceiveMemoryWarning()
		{
			base.DidReceiveMemoryWarning();
			// Release any cached data, images, etc that aren't in use.
		}
	}

	public class SessionDetailTableSource : UITableViewSource
{
	Dictionary<string, string>[]  TableItems;
	static NSString MyCellId = new NSString("sessionDetailCell");

	public SessionDetailTableSource(Dictionary<string, string>[]  items)
	{

		TableItems = items;
	}

	public override nint RowsInSection(UITableView tableview, nint section)
	{
		return TableItems.Length;
	}

	public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
	{


			var cell = (SessionDetailTableCell)tableView.DequeueReusableCell(MyCellId, indexPath);
			Dictionary<string, string> item = TableItems[indexPath.Row];
		//cell.TextLabel.Text = item;
		cell.SelectionStyle = UITableViewCellSelectionStyle.None;
			cell.setInitialData(item);
		//cell.SetThumbImage(UIImage.FromFile("29_icon.png"), UIControlState.Normal);
		return cell;
	}
}




}

