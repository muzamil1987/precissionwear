﻿using System;

using Foundation;
using UIKit;

namespace Alex.iOS
{
	public partial class SportCell : UITableViewCell
	{
		PickerType selectedPickerType;
		public static readonly NSString Key = new NSString("SportCell");
		public static readonly UINib Nib;

		static SportCell()
		{
			Nib = UINib.FromName("SportCell", NSBundle.MainBundle);
		}

		protected SportCell(IntPtr handle) : base(handle)
		{
			// Note: this .ctor should not contain any initialization logic.
		}

		public override void AwakeFromNib()
		{
			base.AwakeFromNib();

			nfloat kFONT_WIDTH_FACTOR = UIScreen.MainScreen.Bounds.Width / 414;
			txtPrimaryPosition.Font = UIFont.FromName("Arial", kFONT_WIDTH_FACTOR * 12f);
			txtSecondaryPosition.Font = UIFont.FromName("Arial", kFONT_WIDTH_FACTOR * 12f);

			btnLevelTeam.TitleLabel.Font = UIFont.FromName("Arial", kFONT_WIDTH_FACTOR * 12f);
			btnSportType.TitleLabel.Font = UIFont.FromName("Arial", kFONT_WIDTH_FACTOR * 12f);
			btnLevelSport.TitleLabel.Font = UIFont.FromName("Arial", kFONT_WIDTH_FACTOR * 12f);
			btnYearsPlaying.TitleLabel.Font = UIFont.FromName("Arial", kFONT_WIDTH_FACTOR * 12f);


			btnLevelTeam.ImageEdgeInsets = new UIEdgeInsets(0, btnLevelTeam.Frame.Size.Width - 40, 0, 0);
			btnSportType.ImageEdgeInsets = new UIEdgeInsets(0, btnSportType.Frame.Size.Width - 40, 0, 0);
			btnLevelSport.ImageEdgeInsets = new UIEdgeInsets(0, btnLevelSport.Frame.Size.Width - 40, 0, 0);
			btnYearsPlaying.ImageEdgeInsets = new UIEdgeInsets(0, btnYearsPlaying.Frame.Size.Width - 40, 0, 0);

			NSNotificationCenter.DefaultCenter.RemoveObserver(this);
			NSNotificationCenter.DefaultCenter.AddObserver ((NSString)"SetSportsPickerData", SetPickerData);

		}
		public void setInitialData()
		{
			UIView heightPaddingView = new UIView();
			heightPaddingView.Frame = new CoreGraphics.CGRect(0, 0, 10, txtPrimaryPosition.Frame.Size.Height);
			txtPrimaryPosition.LeftView = heightPaddingView;
			txtPrimaryPosition.LeftViewMode = UITextFieldViewMode.Always;

			UIView weightPaddingView = new UIView();
			weightPaddingView.Frame = new CoreGraphics.CGRect(0, 0, 10, txtSecondaryPosition.Frame.Size.Height);
			txtSecondaryPosition.LeftView = weightPaddingView;
			txtSecondaryPosition.LeftViewMode = UITextFieldViewMode.Always;

		}

		public void SetPickerData(NSNotification notification)
		{
			NSNotificationCenter.DefaultCenter.RemoveObserver(this);
			if (notification.Object == null)
			{
				return;
			}
			Console.Write("PICKER DATA");
			NSString title = (NSString)notification.Object;
			if (selectedPickerType == PickerType.TypeOfSport)
			{
				btnSportType.SetTitle(title, UIControlState.Normal);
			}
			else if (selectedPickerType == PickerType.YearsPlayingSport)
			{
				btnYearsPlaying.SetTitle(title, UIControlState.Normal);
			}
			else if (selectedPickerType == PickerType.LevelAtSport)
			{
				btnLevelSport.SetTitle(title, UIControlState.Normal);
			}
			else if (selectedPickerType == PickerType.LevelAtTeam)
			{
				btnLevelTeam.SetTitle(title, UIControlState.Normal);
			}
		}
		partial void SportPickerView(UIButton sender)
		{
			int tag = (int)sender.Tag;
			selectedPickerType = (PickerType)tag;
			NSNotificationCenter.DefaultCenter.PostNotificationName((NSString)"showPickerView", NSNumber.FromNInt(sender.Tag));
		}
	}
}