﻿using System;

using Foundation;
using UIKit;

namespace Alex.iOS
{
	public partial class CreateSessionCell : UITableViewCell
	{
		public static readonly NSString Key = new NSString("CreateSessionCell");
		public static readonly UINib Nib;

		static CreateSessionCell()
		{
			Nib = UINib.FromName("CreateSessionCell", NSBundle.MainBundle);
		}

		protected CreateSessionCell(IntPtr handle) : base(handle)
		{
			// Note: this .ctor should not contain any initialization logic.
		}
		public override void AwakeFromNib()
		{
			base.AwakeFromNib();
			nfloat kFONT_WIDTH_FACTOR = UIScreen.MainScreen.Bounds.Width / 414;

			UIView heightPaddingView = new UIView();
			heightPaddingView.Frame = new CoreGraphics.CGRect(0, 0, 10, txtSessionName.Frame.Size.Height);
			txtSessionName.LeftView = heightPaddingView;
			txtSessionName.LeftViewMode = UITextFieldViewMode.Always;

			//txtHeight.Font = UIFont.FromName("Arial", kFONT_WIDTH_FACTOR * 12f);
			//btnLowerBody.TitleLabel.Font = UIFont.FromName("Arial", kFONT_WIDTH_FACTOR * 12f);

	//Console.WriteLine("");

	//btnLowerBody.SetNeedsLayout();
	//btnUpperBody.SetNeedsLayout();
	//btnLowerBody.SetNeedsLayout();
	//btnUpperBody.SetNeedsLayout();

			//btnStartTime.ImageEdgeInsets = new UIEdgeInsets(0, 0, 0, 0);
			//btnStartedElapsed.ImageEdgeInsets = new UIEdgeInsets(0, btnStartedElapsed.Frame.Size.Width - 40, 0, 0);
			//=/btnFieldSize.ImageEdgeInsets = new UIEdgeInsets(0, btnFieldSize.Frame.Size.Width - 40, 0, 0);
			//btnSessionType.ImageEdgeInsets = new UIEdgeInsets(0, btnSessionType.Frame.Size.Width - 40, 0, 0);
	//btnUpperBody.ImageEdgeInsets = new UIEdgeInsets(0, btnUpperBody.Frame.Size.Width - 40, 0, 0)		
		}

	}
}
