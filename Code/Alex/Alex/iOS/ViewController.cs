﻿using System;

using UIKit;

namespace Alex.iOS
{
	public partial class ViewController : UIViewController
	{
		int count = 1;

		public ViewController(IntPtr handle) : base(handle)
		{
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			//this.NavigationController.NavigationBar.SetBackgroundImage(UIImage.FromBundle(""), UIBarMetrics.Default);
			//this.NavigationController.NavigationBar.ShadowImage = UIImage.FromBundle("");

			NavigationController.NavigationBar.SetBackgroundImage(new UIImage (), UIBarMetrics.Default);
			NavigationController.NavigationBar.ShadowImage = new UIImage ();

			//self.navigationController?.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
//self.navigationController?.navigationBar.shadowImage = UIImage()

			// Perform any additional setup after loading the view, typically from a nib.
			//Button.AccessibilityIdentifier = "myButton";
			//Button.TouchUpInside += delegate
			//{
			//	var title = string.Format("{0} clicks!", count++);
			//	Button.SetTitle(title, UIControlState.Normal);
			//};
		}

		public override void DidReceiveMemoryWarning()
		{
			base.DidReceiveMemoryWarning();
			// Release any cached data, images, etc that aren't in use.		
		}
	}
}
