﻿using System;

using UIKit;
using Foundation;

namespace Alex.iOS
{
	public partial class BiometricController : UIViewController
	{
		public BiometricController(IntPtr handle) : base(handle)
		{
		}
		public BiometricController() : base("BiometricController", null)
		{
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();



		string[] tableItems = new string[] { "Vegetables"};
		tblListing.Source = new TableSourceBiometric(tableItems);

			// Perform any additional setup after loading the view, typically from a nib.
		}

		public override void DidReceiveMemoryWarning()
		{
			base.DidReceiveMemoryWarning();
			// Release any cached data, images, etc that aren't in use.
		}
	}



public class TableSourceBiometric : UITableViewSource
	{
	string[] TableItems;
	static NSString MyCellId = new NSString("biometricCell");

	public TableSourceBiometric(string[] items)
	{
			
		TableItems = items;
	}

	public override nint RowsInSection(UITableView tableview, nint section)
	{
		return TableItems.Length;
	}

	public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
	{


			var cell = (BiometricCell)tableView.DequeueReusableCell(MyCellId, indexPath);
			string item = TableItems[indexPath.Row];
			//cell.TextLabel.Text = item;
			cell.SelectionStyle = UITableViewCellSelectionStyle.None;
			cell.setInitialData();
			//cell.SetThumbImage(UIImage.FromFile("29_icon.png"), UIControlState.Normal);
			return cell;
	}
}
}



